package ru.faqdji.spark.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import ru.faqdji.spark.R;
import ru.faqdji.spark.db.entity.PointAction;
import ru.faqdji.spark.dji.WaypointActionType;
import ru.faqdji.spark.ui.adapter.ActionRvAdapter;
import ru.faqdji.spark.utils.InputFilterMinMax;
import ru.faqdji.spark.utils.PointActionWrapper;
import ru.faqdji.spark.ui.viewmodel.WayPointViewModel;

public class WayPointActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.actions_rv) RecyclerView actionsRv;
    private WayPointViewModel wayPointViewModel;
    private ActionRvAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_point);
        ButterKnife.bind(this);
        wayPointViewModel = ViewModelProviders.of(this).get(WayPointViewModel.class);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        actionsRv.setLayoutManager(mLayoutManager);
        adapter = new ActionRvAdapter(wayPointViewModel, this);
        actionsRv.setAdapter(adapter);

        if (wayPointViewModel.getActions().size() == 0) {
            PointActionWrapper wrapper = (PointActionWrapper) getIntent().getSerializableExtra("actions");
            wayPointViewModel.setActions(wrapper.getPointActions());
            String pointId = getIntent().getStringExtra("pointId");
            wayPointViewModel.setPointId(pointId);
        }

        wayPointViewModel.isDataChanged().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                List<PointAction> list = wayPointViewModel.getActions();
                adapter.setData(list);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_waypoint, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (wayPointViewModel.isNeedSave()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.need_save_title);
            builder.setMessage(R.string.save_before_exit);
            builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    saveAndFinish();
                }
            });
            builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            builder.create().show();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_done) {
            saveAndFinish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveAndFinish() {
        Intent intent = new Intent();
        intent.putExtra("pointId", wayPointViewModel.getPointId());
        intent.putExtra("actions", new PointActionWrapper(wayPointViewModel.getActions()));
        setResult(RESULT_OK, intent);
        finish();
    }

    @OnClick(R.id.addActionButton)
    public void addActionClick() {
        LinearLayout wayPointActionSettings = (LinearLayout) getLayoutInflater().inflate(R.layout.dialog_add_waipoint_action, null);
        final Spinner typesNameSpinner = wayPointActionSettings.findViewById(R.id.types_name);
        final LinearLayout paramLayout = wayPointActionSettings.findViewById(R.id.param_layout);
        final EditText paramValue = wayPointActionSettings.findViewById(R.id.param_value);
        final TextView paramLabel = wayPointActionSettings.findViewById(R.id.param_label);
        List<String> data = new ArrayList<>();
        data.add(getString(R.string.action_stay));
        data.add(getString(R.string.action_start_take_photo));
        data.add(getString(R.string.action_start_record));
        data.add(getString(R.string.action_stop_record));
        data.add(getString(R.string.action_rotate_aicraft));
        data.add(getString(R.string.action_gimbal_pitch));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data);
        typesNameSpinner.setAdapter(adapter);
        typesNameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0 || position == 4 || position == 5) {
                    paramLayout.setVisibility(View.VISIBLE);
                    if (position == 0) {
                        paramValue.setFilters(new InputFilter[]{new InputFilterMinMax(0, 32767)});
                        paramLabel.setText(getString(R.string.milliseconds));
                    } else if (position == 4) {
                        paramValue.setFilters(new InputFilter[]{new InputFilterMinMax(-180, 180)});
                        paramLabel.setText(getString(R.string.degrees));
                    } else {
                        paramLabel.setText(getString(R.string.degrees));
                        paramValue.setFilters(new InputFilter[]{new InputFilterMinMax(-90, 0)});
                    }
                } else {
                    paramLayout.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        typesNameSpinner.setSelection(0);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add);
        builder.setView(wayPointActionSettings);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int selection = typesNameSpinner.getSelectedItemPosition();
                int type = 0;
                int value = 0;
                if (selection == 0) {
                    type = WaypointActionType.STAY.value();
                    value = Integer.parseInt(paramValue.getText().toString());
                } else if (selection == 1) {
                    type = WaypointActionType.START_TAKE_PHOTO.value();
                } else if (selection == 2) {
                    type = WaypointActionType.START_RECORD.value();
                } else if (selection == 3) {
                    type = WaypointActionType.STOP_RECORD.value();
                } else if (selection == 4) {
                    type = WaypointActionType.ROTATE_AIRCRAFT.value();
                    value = Integer.parseInt(paramValue.getText().toString());
                } else if (selection == 5) {
                    type = WaypointActionType.GIMBAL_PITCH.value();
                    value = Integer.parseInt(paramValue.getText().toString());
                }
                PointAction pointAction = new PointAction();
                pointAction.setId(UUID.randomUUID().toString());
                pointAction.setPointId(wayPointViewModel.getPointId());
                pointAction.setActionType(type);
                pointAction.setActionParam(value);
                wayPointViewModel.addAction(pointAction);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

}
