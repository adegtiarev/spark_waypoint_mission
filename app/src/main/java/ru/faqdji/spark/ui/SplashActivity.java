package ru.faqdji.spark.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import ru.faqdji.spark.R;
import ru.faqdji.spark.utils.permissions.PermissionUtil;
import ru.faqdji.spark.utils.permissions.ShortPerCallback;

public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissions();
        } else {
            startMainActivity();
        }
    }

    private void startMainActivity() {
        Intent homeIntent = new Intent(this, MainActivity.class);
        startActivity(homeIntent);
        finish();
    }

    private void checkPermissions() {
        PermissionUtil.checkGroup(this, new ShortPerCallback() {
            @Override
            public void onPermissionGranted() {
                super.onPermissionGranted();
                startMainActivity();
            }

            @Override
            public void onPermissionDenied() {
                super.onPermissionDenied();
                Toast.makeText(SplashActivity.this, R.string.permission_denied, Toast.LENGTH_LONG).show();
                finish();
            }
        }, new String[]{
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,

                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION,
        });
    }
}
