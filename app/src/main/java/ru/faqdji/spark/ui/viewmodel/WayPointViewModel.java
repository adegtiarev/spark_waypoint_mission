package ru.faqdji.spark.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import ru.faqdji.spark.db.entity.PointAction;

public class WayPointViewModel extends ViewModel {
    private MutableLiveData<Boolean> dataChanged = new MutableLiveData<>();
    private String pointId;
    private boolean needSave = false;

    private List<PointAction> actions = new ArrayList<>();

    public List<PointAction> getActions() {
        return actions;
    }

    public void setActions(List<PointAction> actions) {
        this.actions = actions;
        dataChanged.postValue(true);
    }

    public MutableLiveData<Boolean> isDataChanged() {
        return dataChanged;
    }

    public void addAction(PointAction pointAction) {
        actions.add(pointAction);
        needSave = true;
        dataChanged.postValue(true);
    }

    public void deleteAction(PointAction pointAction) {
        actions.remove(pointAction);
        needSave = true;
        dataChanged.postValue(true);
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    public boolean isNeedSave() {
        return needSave;
    }

}
