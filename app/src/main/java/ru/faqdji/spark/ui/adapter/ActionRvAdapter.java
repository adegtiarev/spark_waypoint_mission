package ru.faqdji.spark.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.faqdji.spark.R;
import ru.faqdji.spark.db.entity.PointAction;
import ru.faqdji.spark.dji.WaypointActionType;
import ru.faqdji.spark.ui.custom.SwipeRevealLayout;
import ru.faqdji.spark.ui.viewmodel.WayPointViewModel;

public class ActionRvAdapter extends RecyclerView.Adapter<ActionRvAdapter.ViewHolder> {
    private static final String TAG = ActionRvAdapter.class.getName();

    private List<PointAction> items = new ArrayList<>();
    private WayPointViewModel wayPointViewModel;
    private Context context;

    public ActionRvAdapter(WayPointViewModel wayPointViewModel, Context context) {
        this.wayPointViewModel = wayPointViewModel;
        this.context = context;
    }

    public void setData(List<PointAction> data) {
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public ActionRvAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waypoint_action, parent, false);
        return new ActionRvAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PointAction pointAction = items.get(position);
        holder.bindItem(pointAction);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private PointAction pointAction;

        @BindView(R.id.swl) SwipeRevealLayout swipeLayout;
        @BindView(R.id.remove) ImageView removeButton;
        @BindView(R.id.bottom_wrapper) View bottomWrapper;
        @BindView(R.id.action_item) View actionItem;

        @BindView(R.id.action_name) TextView actionName;
        @BindView(R.id.action_value) TextView actionValue;
        @BindView(R.id.actions_value_label) TextView label;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    swipeLayout.close(true);
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            wayPointViewModel.deleteAction(pointAction);
                        }
                    });
                    thread.start();
                }
            });
        }

        public void bindItem(PointAction pointAction) {
            this.pointAction = pointAction;
            actionName.setText(getActionName(pointAction.getActionType()));
            actionValue.setText(paramValue(pointAction));
            label.setText(getActionValueLabel(pointAction.getActionType()));
        }
    }

    private String paramValue(PointAction pointAction) {
        if (pointAction.getActionType() == WaypointActionType.STAY.value()) {
            return "" + pointAction.getActionParam();
        } else if (pointAction.getActionType() == WaypointActionType.START_TAKE_PHOTO.value()) {
            return "";
        } else if (pointAction.getActionType() == WaypointActionType.START_RECORD.value()) {
            return "";
        } else if (pointAction.getActionType() == WaypointActionType.STOP_RECORD.value()) {
            return "";
        } else if (pointAction.getActionType() == WaypointActionType.ROTATE_AIRCRAFT.value()) {
            return "" + pointAction.getActionParam();
        } else if (pointAction.getActionType() == WaypointActionType.GIMBAL_PITCH.value()) {
            return "" + pointAction.getActionParam();
        } else {
            return context.getString(R.string.unknown);
        }
    }

    private String getActionName(int actionType) {
        if (actionType == WaypointActionType.STAY.value()) {
            return context.getString(R.string.action_stay);
        } else if (actionType == WaypointActionType.START_TAKE_PHOTO.value()) {
            return context.getString(R.string.action_start_take_photo);
        } else if (actionType == WaypointActionType.START_RECORD.value()) {
            return context.getString(R.string.action_start_record);
        } else if (actionType == WaypointActionType.STOP_RECORD.value()) {
            return context.getString(R.string.action_stop_record);
        } else if (actionType == WaypointActionType.ROTATE_AIRCRAFT.value()) {
            return context.getString(R.string.action_rotate_aicraft);
        } else if (actionType == WaypointActionType.GIMBAL_PITCH.value()) {
            return context.getString(R.string.action_gimbal_pitch);
        } else {
            return context.getString(R.string.unknown);
        }
    }

    private String getActionValueLabel(int actionType) {
        if (actionType == WaypointActionType.STAY.value()) {
            return context.getString(R.string.milliseconds);
        } else if (actionType == WaypointActionType.START_TAKE_PHOTO.value()) {
            return "";
        } else if (actionType == WaypointActionType.START_RECORD.value()) {
            return "";
        } else if (actionType == WaypointActionType.STOP_RECORD.value()) {
            return "";
        } else if (actionType == WaypointActionType.ROTATE_AIRCRAFT.value()) {
            return context.getString(R.string.degrees);
        } else if (actionType == WaypointActionType.GIMBAL_PITCH.value()) {
            return context.getString(R.string.degrees);
        } else {
            return context.getString(R.string.unknown);
        }
    }
}
