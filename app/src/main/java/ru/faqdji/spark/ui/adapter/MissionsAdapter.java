package ru.faqdji.spark.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import ru.faqdji.spark.App;
import ru.faqdji.spark.R;
import ru.faqdji.spark.db.entity.MissionEntity;
import ru.faqdji.spark.ui.MainActivity;
import ru.faqdji.spark.ui.MissionActivity;
import ru.faqdji.spark.ui.custom.SwipeRevealLayout;

public class MissionsAdapter extends RecyclerView.Adapter<MissionsAdapter.ViewHolder> {
    private static final String TAG = MissionsAdapter.class.getName();
    private List<MissionEntity> items = new ArrayList<>();
    private static SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
    private Context context;

    public MissionsAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<MissionEntity> data) {
        items.clear();
        items.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mission, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MissionEntity missionEntity = items.get(position);
        holder.bindItem(missionEntity);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private MissionEntity missionEntity;

        @BindView(R.id.swl) SwipeRevealLayout swipeLayout;
        @BindView(R.id.mission_name) TextView missionName;
        @BindView(R.id.bottom_wrapper) View bottomWrapper;
        @BindView(R.id.record_item) View recordItem;
        @BindView(R.id.remove) ImageView removeButton;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    swipeLayout.close(true);
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            App.getInstance().getDatabase().getMissionsDao().delete(missionEntity);
                        }
                    });
                    thread.start();
                }
            });
            recordItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openMission(missionEntity);
                }
            });
        }

        public void bindItem(MissionEntity missionEntity) {
            this.missionEntity = missionEntity;
            missionName.setText(missionEntity.getName());
        }
    }

    private void openMission(MissionEntity missionEntity) {
        Intent intent = new Intent(context, MissionActivity.class);
        intent.putExtra("editMode", false);
        intent.putExtra("missionId", missionEntity.getId());
        ((Activity) context).startActivityForResult(intent, MainActivity.MISSION_REQUEST);
    }
}
