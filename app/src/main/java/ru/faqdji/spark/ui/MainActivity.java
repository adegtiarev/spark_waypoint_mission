package ru.faqdji.spark.ui;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.faqdji.spark.App;
import ru.faqdji.spark.R;
import ru.faqdji.spark.db.entity.MissionEntity;
import ru.faqdji.spark.ui.adapter.MissionsAdapter;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();

    public static final int MISSION_REQUEST = 11;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.missions_rv) RecyclerView missionsRv;

    private MissionsAdapter missionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        missionsAdapter = new MissionsAdapter(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MissionActivity.class);
                intent.putExtra("editMode", true);
                startActivityForResult(intent, MainActivity.MISSION_REQUEST);
            }
        });

        App.getInstance().getDatabase().getMissionsDao().getAll().observe(this, new Observer<List<MissionEntity>>() {
            @Override
            public void onChanged(@Nullable List<MissionEntity> missionEntities) {
                missionsAdapter.setData(missionEntities);
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        missionsRv.setLayoutManager(mLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(missionsRv.getContext(), mLayoutManager.getOrientation());
        missionsRv.addItemDecoration(dividerItemDecoration);

        missionsRv.setAdapter(missionsAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
