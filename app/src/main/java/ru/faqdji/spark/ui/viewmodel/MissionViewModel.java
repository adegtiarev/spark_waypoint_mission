package ru.faqdji.spark.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.location.Location;
import android.util.Log;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Marker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import ru.faqdji.spark.App;
import ru.faqdji.spark.R;
import ru.faqdji.spark.db.entity.MissionEntity;
import ru.faqdji.spark.db.entity.MissionPoint;
import ru.faqdji.spark.db.entity.PointAction;
import ru.faqdji.spark.dji.WaypointActionType;
import ru.faqdji.spark.dji.WaypointMissionFinishedAction;
import ru.faqdji.spark.dji.WaypointMissionHeadingMode;


public class MissionViewModel extends ViewModel {
    private static final String TAG = MissionViewModel.class.getName();

    private MutableLiveData<Boolean> editMode = new MutableLiveData<>();
    private MutableLiveData<Boolean> repaint = new MutableLiveData<>();
    private MutableLiveData<String> missionName = new MutableLiveData<>();
    private MutableLiveData<Integer> countPoints = new MutableLiveData<>();
    private MutableLiveData<Integer> distance = new MutableLiveData<>();

    private static SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private MissionEntity currentMission;
    private List<MissionPoint> missionPoints = new ArrayList<>();
    private List<PointAction> pointActions = new ArrayList<>();
    private boolean newMission = false;
    private MissionPoint draggablePoint;

    public void setEditMode(boolean edit) {
        if (!edit && editMode.getValue() != null) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (currentMission.getDateCreated() > 0) {
                        App.getInstance().getDatabase().getMissionsDao().delete(currentMission);
                        currentMission.setDateUpdated(System.currentTimeMillis());
                        App.getInstance().getDatabase().getMissionsDao().insert(currentMission);
                    } else {
                        currentMission.setDateCreated(System.currentTimeMillis());
                        currentMission.setDateUpdated(System.currentTimeMillis());
                        App.getInstance().getDatabase().getMissionsDao().insert(currentMission);
                    }
                    savePoints();
                    missionName.postValue(currentMission.getName());
                }
            });
            t.start();
            newMission = false;
        }
        editMode.postValue(edit);
    }

    private void savePoints() {
        for (int i = 0; i < missionPoints.size(); i++) {
            MissionPoint missionPoint = missionPoints.get(i);
            missionPoint.setPosition(i);
            App.getInstance().getDatabase().getMissionPointDao().insert(missionPoint);
        }

        Log.i(TAG, pointActions.toString());
        for (int i = 0; i < pointActions.size(); i++) {
            PointAction pointAction = pointActions.get(i);
            pointAction.setPosition(i);
            App.getInstance().getDatabase().getPointActionDao().insert(pointAction);
        }
    }

    public MissionEntity getCurrentMission() {
        return currentMission;
    }

    public void setMission(final String missionId) {
        if (missionId == null) {
            currentMission = new MissionEntity();
            currentMission.setId(UUID.randomUUID().toString());
            currentMission.setName(App.getInstance().getString(R.string.mission) + " " + fmt.format(new Date()));
            currentMission.setSpeed(5.0f);
            currentMission.setFinishedAction(WaypointMissionFinishedAction.NO_ACTION.value());
            currentMission.setHeadingMode(WaypointMissionHeadingMode.AUTO.value());
            newMission = true;
            missionName.postValue(currentMission.getName());
            countPoints.postValue(missionPoints.size());
        } else {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    currentMission = App.getInstance().getDatabase().getMissionsDao().findById(missionId);
                    missionPoints = App.getInstance().getDatabase().getMissionPointDao().getAllByMission(missionId);
                    pointActions = App.getInstance().getDatabase().getPointActionDao().getAllByMissionId(missionId);
                    repaint.postValue(true);
                    Log.i(TAG, pointActions.toString());
                    missionName.postValue(currentMission.getName());
                    countPoints.postValue(missionPoints.size());
                    distance.postValue(calcDistance());
                }
            });
            t.start();
        }
    }

    public List<MissionPoint> getMissionPoints() {
        return missionPoints;
    }

    public boolean isNewMission() {
        return newMission;
    }

    public String addMarker(GeoPoint p) {
        MissionPoint missionPoint = new MissionPoint();
        missionPoint.setId(UUID.randomUUID().toString());
        missionPoint.setMissionId(currentMission.getId());

        missionPoint.setAltitude(10);
        missionPoint.setLongitude(p.getLongitude());
        missionPoint.setLatitude(p.getLatitude());
        missionPoints.add(missionPoint);

        countPoints.postValue(missionPoints.size());
        distance.postValue(calcDistance());

        return missionPoint.getId();
    }

    public void startDraggable(Marker marker) {
        for (MissionPoint missionPoint : missionPoints) {
            if (missionPoint.getId().equals(marker.getTitle())) {
                draggablePoint = missionPoint;
                break;
            }
        }
    }

    public void endDraggable(Marker marker) {
        draggablePoint.setLatitude(marker.getPosition().getLatitude());
        draggablePoint.setLongitude(marker.getPosition().getLongitude());
        distance.postValue(calcDistance());
    }

    public void removeMarker(Marker marker) {
        for (final MissionPoint missionPoint : missionPoints) {
            if (missionPoint.getId().equals(marker.getTitle())) {
                List<PointAction> actionsM = getActionsByPointId(marker.getTitle());
                pointActions.removeAll(actionsM);
                missionPoints.remove(missionPoint);
                break;
            }
        }
        countPoints.postValue(missionPoints.size());
        distance.postValue(calcDistance());
    }

    private int calcDistance() {
        int result = 0;
        MissionPoint last = null;
        for (MissionPoint missionPoint : missionPoints) {
            if (last == null) {
                last = missionPoint;
                continue;
            }
            Location loc1 = new Location("GPS");
            loc1.setLatitude(last.getLatitude());
            loc1.setLongitude(last.getLongitude());

            Location loc2 = new Location("GPS");
            loc2.setLatitude(missionPoint.getLatitude());
            loc2.setLongitude(missionPoint.getLongitude());

            float f = loc1.distanceTo(loc2);
            result += f;
            last = missionPoint;
        }
        return result;
    }

    public String getAltitudeByMarkerId(String markerId) {
        for (MissionPoint missionPoint : missionPoints) {
            if (missionPoint.getId().equals(markerId)) {
                return Float.toString(missionPoint.getAltitude());
            }
        }
        return "0";
    }

    public void setAltitudeByMarkerId(String markerId, float alt) {
        for (MissionPoint missionPoint : missionPoints) {
            if (missionPoint.getId().equals(markerId)) {
                missionPoint.setAltitude(alt);
                break;
            }
        }
    }

    public String getActionsString(final String id) {

        List<PointAction> list = getActionsByPointId(id);
        String result = "";
        for (PointAction pointAction : list) {
            if (pointAction.getActionType() == WaypointActionType.STAY.value()) {
                result += App.getInstance().getString(R.string.action_stay) + ": " + pointAction.getActionParam() + "\n";
            } else if (pointAction.getActionType() == WaypointActionType.START_TAKE_PHOTO.value()) {
                result += App.getInstance().getString(R.string.action_start_take_photo) + "\n";
            } else if (pointAction.getActionType() == WaypointActionType.START_RECORD.value()) {
                result += App.getInstance().getString(R.string.action_start_record) + "\n";
            } else if (pointAction.getActionType() == WaypointActionType.STOP_RECORD.value()) {
                result += App.getInstance().getString(R.string.action_stop_record) + "\n";
            } else if (pointAction.getActionType() == WaypointActionType.ROTATE_AIRCRAFT.value()) {
                result += App.getInstance().getString(R.string.action_rotate_aicraft) + ": " + pointAction.getActionParam() + "\n";
            } else if (pointAction.getActionType() == WaypointActionType.GIMBAL_PITCH.value()) {
                result += App.getInstance().getString(R.string.action_gimbal_pitch) + ": " + pointAction.getActionParam() + "\n";
            }
        }
        return result;
    }

    public List<PointAction> getActionsByPointId(String id) {
        List<PointAction> actions = new ArrayList<>();
        for (PointAction action : pointActions) {
            if (action.getPointId().equals(id)) {
                actions.add(action);
            }
        }
        return actions;
    }


    public void setNewActions(List<PointAction> newActions, String id) {
        List<PointAction> old = getActionsByPointId(id);
        pointActions.removeAll(old);
        pointActions.addAll(newActions);
    }


    public MutableLiveData<String> getMissionName() {
        return missionName;
    }

    public MutableLiveData<Integer> getCountPoints() {
        return countPoints;
    }

    public MutableLiveData<Integer> getDistance() {
        return distance;
    }

    public MutableLiveData<Boolean> getRepaint() {
        return repaint;
    }

    public MutableLiveData<Boolean> getEditMode() {
        return editMode;
    }
}
