package ru.faqdji.spark.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.faqdji.spark.App;
import ru.faqdji.spark.R;
import ru.faqdji.spark.db.entity.MissionPoint;
import ru.faqdji.spark.dji.WaypointMissionFinishedAction;
import ru.faqdji.spark.dji.WaypointMissionHeadingMode;
import ru.faqdji.spark.utils.PointActionWrapper;
import ru.faqdji.spark.ui.viewmodel.MissionViewModel;

public class MissionActivity extends AppCompatActivity {
    private static final int REQUEST_CHANGE_ACTIONS = 1;

    @BindView(R.id.mission_name) TextView missionNameTv;
    @BindView(R.id.count_points) TextView countPointsTv;
    @BindView(R.id.distance) TextView distanceTv;
    @BindView(R.id.settings) ImageView settingsButton;
    @BindView(R.id.map) MapView mapView;

    private boolean detect = false;
    private MissionViewModel missionViewModel;
    private boolean editMode = false;

    private ArrayList<Marker> markers = new ArrayList<>();
    private Polyline line = new Polyline();
    private List<GeoPoint> geoPoints = new ArrayList<>();
    private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mission);
        ButterKnife.bind(this);

        missionViewModel = ViewModelProviders.of(this).get(MissionViewModel.class);

        app = App.getInstance();
        Configuration.getInstance().load(App.getInstance(), PreferenceManager.getDefaultSharedPreferences(App.getInstance()));

        initMapView();
        initMyLocation();

        if (missionViewModel.getCurrentMission() == null) {
            String missionId = getIntent().getStringExtra("missionId");
            missionViewModel.setMission(missionId);
        } else {
            repaintMission();
        }

        if (missionViewModel.getEditMode().getValue() == null) {
            editMode = getIntent().getBooleanExtra("editMode", false);
            missionViewModel.setEditMode(editMode);
        }

        setObservers();
        if (savedInstanceState != null) {
            int zoom = savedInstanceState.getInt("zoom");
            double lat = savedInstanceState.getDouble("lat");
            double lon = savedInstanceState.getDouble("lon");
            cameraGoTo(lat, lon, zoom);
        } else {
            detect = true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("zoom", mapView.getZoomLevel());
        outState.putDouble("lat", mapView.getMapCenter().getLatitude());
        outState.putDouble("lon", mapView.getMapCenter().getLongitude());
        super.onSaveInstanceState(outState);
    }

    private void setObservers() {
//        app.getLocationLiveData().observe(this, new Observer<Location>() {
//            @Override
//            public void onChanged(@Nullable Location location) {
//                if (detect && location != null) {
//                    cameraGoTo(location.getLatitude(), location.getLongitude(), 15);
//                    detect = false;
//                }
//            }
//        });

        missionViewModel.getEditMode().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean != null) {
                    editMode = aBoolean;
                    invalidateOptionsMenu();
                    if (editMode) {
                        if (missionViewModel.isNewMission()) {
                            setTitle(getString(R.string.new_mission));
                        } else {
                            setTitle(getString(R.string.edit_mission));
                        }
                    } else {
                        String s = missionViewModel.getCurrentMission().getName();
                        setTitle(s != null ? s : getString(R.string.mission));
                    }
                    for (Marker marker : markers) {
                        marker.setDraggable(editMode);
                    }
                }
            }
        });

        missionViewModel.getRepaint().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                repaintMission();
            }
        });

        missionViewModel.getMissionName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                missionNameTv.setText(s);
            }
        });

        missionViewModel.getCountPoints().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                countPointsTv.setText(integer.toString());
            }
        });

        missionViewModel.getDistance().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                distanceTv.setText(getString(R.string.dist_meters, integer.toString()));
            }
        });

    }

    private void cameraGoTo(double aLatitude, double aLongitude, int zoom) {
        IMapController mapController = mapView.getController();
        mapController.setZoom(zoom);
        GeoPoint startPoint = new GeoPoint(aLatitude, aLongitude);
        mapController.animateTo(startPoint);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_mission, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_edit).setVisible(!editMode);
        menu.findItem(R.id.action_done).setVisible(editMode);
        menu.findItem(R.id.action_upload).setVisible(!editMode);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_done) {
            missionViewModel.setEditMode(false);
            return true;
        } else if (id == R.id.action_edit) {
            missionViewModel.setEditMode(true);
            return true;
        } else if (id == R.id.action_upload) {
            if (missionViewModel.getMissionPoints().size() > 0) {
                Toast.makeText(app, R.string.not_work_yet, Toast.LENGTH_LONG).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        Configuration.getInstance().load(App.getInstance(), PreferenceManager.getDefaultSharedPreferences(App.getInstance()));
    }


    private void initMapView() {
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setMultiTouchControls(true);
        mapView.setTilesScaledToDpi(true);

        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                if (editMode) {
                    String id = missionViewModel.addMarker(p);
                    addMarker(p, id);
                }
                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {
                return false;
            }
        };


        MapEventsOverlay overlayEvents = new MapEventsOverlay(mReceive);
        mapView.getOverlays().add(overlayEvents);
        mapView.getOverlayManager().add(line);

    }

    public void addMarker(GeoPoint p, String wayPointId) {
        geoPoints.add(p);
        line.setPoints(geoPoints);

        Marker marker = new Marker(mapView);
        marker.setOnMarkerDragListener(new Marker.OnMarkerDragListener() {
            @Override
            public void onMarkerDrag(Marker marker) {
                geoPoints.clear();
                for (Marker m : markers) {
                    geoPoints.add(m.getPosition());
                    line.setPoints(geoPoints);
                    mapView.invalidate();
                }
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                missionViewModel.endDraggable(marker);
            }

            @Override
            public void onMarkerDragStart(Marker marker) {
                missionViewModel.startDraggable(marker);
                if (Build.VERSION.SDK_INT >= 26) {
                    ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(50);
                }
            }
        });
        marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker, final MapView mapView) {
                showMarkerDialog(marker);
                return true;
            }
        });
        marker.setDraggable(editMode);
        marker.setPosition(p);
        marker.setTitle(wayPointId);
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        markers.add(marker);
        mapView.getOverlays().add(marker);
        changeIcons();
    }

    private void changeIcons() {
        for (int i = 0; i < markers.size(); i++) {
            Marker m = markers.get(i);
            if (i == 0) {
                m.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_location_on_green_800_48dp));
            } else if (i == (markers.size() - 1)) {
                m.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_location_on_red_800_48dp));
            } else {
                m.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_location_on_amber_a700_48dp));
            }
        }
        mapView.invalidate();
    }

    private void showMarkerDialog(final Marker marker) {
        ScrollView wayPointSettings = (ScrollView) getLayoutInflater().inflate(R.layout.dialog_waypointsetting, null);

        final EditText altEt = wayPointSettings.findViewById(R.id.altitude_et);
        TextView changeButton = wayPointSettings.findViewById(R.id.change_button);
        TextView actions = wayPointSettings.findViewById(R.id.actions);
        actions.setText(missionViewModel.getActionsString(marker.getTitle()));
        altEt.setEnabled(editMode);
        changeButton.setEnabled(editMode);

        altEt.setText(missionViewModel.getAltitudeByMarkerId(marker.getTitle()));
        altEt.setSelection(altEt.getText().length());
        AlertDialog.Builder builder = new AlertDialog.Builder(MissionActivity.this);
        builder.setTitle(R.string.waypoint_settings);
        builder.setView(wayPointSettings);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (editMode && altEt.getText().length() > 0) {
                    missionViewModel.setAltitudeByMarkerId(marker.getTitle(), Float.parseFloat(altEt.getText().toString()));
                }
                dialogInterface.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        if (editMode) {
            builder.setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    markers.remove(marker);
                    geoPoints.remove(marker.getPosition());
                    line.setPoints(geoPoints);
                    missionViewModel.removeMarker(marker);
                    mapView.getOverlays().remove(marker);
                    changeIcons();
                }
            });
        }
        final AlertDialog dialog = builder.create();

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MissionActivity.this, WayPointActivity.class);
                intent.putExtra("pointId", marker.getTitle());
                intent.putExtra("actions", new PointActionWrapper(missionViewModel.getActionsByPointId(marker.getTitle())));
                startActivityForResult(intent, REQUEST_CHANGE_ACTIONS);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHANGE_ACTIONS && resultCode == RESULT_OK) {
            PointActionWrapper wrapper = (PointActionWrapper) data.getSerializableExtra("actions");
            String id = data.getStringExtra("pointId");
            missionViewModel.setNewActions(wrapper.getPointActions(), id);
        }
    }

    private void repaintMission() {
        if (markers.size() > 0) {
            return;
        }
        List<MissionPoint> missionPoints = missionViewModel.getMissionPoints();
        boolean needLocate = true;
        for (MissionPoint missionPoint : missionPoints) {
            if (needLocate) {
                cameraGoTo(missionPoint.getLatitude(), missionPoint.getLongitude(), 15);
                needLocate = false;
            }
            GeoPoint geoPoint = new GeoPoint(missionPoint.getLatitude(), missionPoint.getLongitude(), missionPoint.getAltitude());
            addMarker(geoPoint, missionPoint.getId());
        }
    }

    private void initMyLocation() {
        MyLocationNewOverlay mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(this), mapView);
        mLocationOverlay.enableMyLocation();
        mapView.getOverlays().add(mLocationOverlay);
    }

    @OnClick(R.id.settings)
    public void missionSettingsClick(View view) {
        LinearLayout missionConfigDialogView = (LinearLayout) getLayoutInflater().inflate(R.layout.dialog_mission_config, null);
        setEnabled(missionConfigDialogView);

        final EditText misName = missionConfigDialogView.findViewById(R.id.mis_name);
        misName.setText(missionViewModel.getCurrentMission().getName());
        misName.setSelection(missionViewModel.getCurrentMission().getName().length());

        final RadioGroup speedRG = missionConfigDialogView.findViewById(R.id.speed);
        float speed = missionViewModel.getCurrentMission().getSpeed();
        if (speed == 3.0f) {
            speedRG.check(R.id.lowSpeed);
        } else if (speed == 5.0f) {
            speedRG.check(R.id.MidSpeed);
        } else {
            speedRG.check(R.id.HighSpeed);
        }

        final RadioGroup actionAfterFinishedRG = missionConfigDialogView.findViewById(R.id.actionAfterFinished);
        int finAction = missionViewModel.getCurrentMission().getFinishedAction();
        if (finAction == WaypointMissionFinishedAction.NO_ACTION.value()) {
            actionAfterFinishedRG.check(R.id.finishNone);
        } else if (finAction == WaypointMissionFinishedAction.GO_HOME.value()) {
            actionAfterFinishedRG.check(R.id.finishGoHome);
        } else if (finAction == WaypointMissionFinishedAction.AUTO_LAND.value()) {
            actionAfterFinishedRG.check(R.id.finishAutoLanding);
        } else {
            actionAfterFinishedRG.check(R.id.finishToFirst);
        }

        final RadioGroup headingRG = missionConfigDialogView.findViewById(R.id.heading);
        int heading = missionViewModel.getCurrentMission().getHeadingMode();
        if (heading == WaypointMissionHeadingMode.AUTO.value()) {
            headingRG.check(R.id.headingNext);
        } else if (heading == WaypointMissionHeadingMode.USING_INITIAL_DIRECTION.value()) {
            headingRG.check(R.id.headingInitDirec);
        } else if (heading == WaypointMissionHeadingMode.CONTROL_BY_REMOTE_CONTROLLER.value()) {
            headingRG.check(R.id.headingRC);
        } else {
            headingRG.check(R.id.headingWP);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.mission_config);
        builder.setView(missionConfigDialogView);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int checkedId = speedRG.getCheckedRadioButtonId();
                if (checkedId == R.id.lowSpeed) {
                    missionViewModel.getCurrentMission().setSpeed(3.0f);
                } else if (checkedId == R.id.MidSpeed) {
                    missionViewModel.getCurrentMission().setSpeed(5.0f);
                } else if (checkedId == R.id.HighSpeed) {
                    missionViewModel.getCurrentMission().setSpeed(10.0f);
                }

                checkedId = actionAfterFinishedRG.getCheckedRadioButtonId();
                if (checkedId == R.id.finishNone) {
                    missionViewModel.getCurrentMission().setFinishedAction(WaypointMissionFinishedAction.NO_ACTION.value());
                } else if (checkedId == R.id.finishGoHome) {
                    missionViewModel.getCurrentMission().setFinishedAction(WaypointMissionFinishedAction.GO_HOME.value());
                } else if (checkedId == R.id.finishAutoLanding) {
                    missionViewModel.getCurrentMission().setFinishedAction(WaypointMissionFinishedAction.AUTO_LAND.value());
                } else if (checkedId == R.id.finishToFirst) {
                    missionViewModel.getCurrentMission().setFinishedAction(WaypointMissionFinishedAction.GO_FIRST_WAYPOINT.value());
                }

                checkedId = headingRG.getCheckedRadioButtonId();
                if (checkedId == R.id.headingNext) {
                    missionViewModel.getCurrentMission().setHeadingMode(WaypointMissionHeadingMode.AUTO.value());
                } else if (checkedId == R.id.headingInitDirec) {
                    missionViewModel.getCurrentMission().setHeadingMode(WaypointMissionHeadingMode.USING_INITIAL_DIRECTION.value());
                } else if (checkedId == R.id.headingRC) {
                    missionViewModel.getCurrentMission().setHeadingMode(WaypointMissionHeadingMode.CONTROL_BY_REMOTE_CONTROLLER.value());
                } else if (checkedId == R.id.headingWP) {
                    missionViewModel.getCurrentMission().setHeadingMode(WaypointMissionHeadingMode.USING_WAYPOINT_HEADING.value());
                }

                missionViewModel.getCurrentMission().setName(misName.getText().toString());
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        dialog.show();
    }

    private void setEnabled(ViewGroup layout) {
        layout.setEnabled(editMode);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                setEnabled((ViewGroup) child);
            } else {
                child.setEnabled(editMode);
            }
        }
    }
}
