package ru.faqdji.spark.utils.permissions;

public interface PermissionCallback {

    void onPermissionGranted();

    void onPermissionDenied();
}
