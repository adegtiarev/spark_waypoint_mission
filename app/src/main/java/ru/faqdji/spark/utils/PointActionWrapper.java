package ru.faqdji.spark.utils;

import java.io.Serializable;
import java.util.List;

import ru.faqdji.spark.db.entity.PointAction;

public class PointActionWrapper implements Serializable {

    private List<PointAction> pointActions;

    public PointActionWrapper(List<PointAction> pointActions) {
        this.pointActions = pointActions;
    }

    public List<PointAction> getPointActions() {
        return pointActions;
    }

    public void setPointActions(List<PointAction> pointActions) {
        this.pointActions = pointActions;
    }
}
