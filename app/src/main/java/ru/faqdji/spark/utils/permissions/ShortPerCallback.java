package ru.faqdji.spark.utils.permissions;

public abstract class ShortPerCallback implements PermissionCallback {

    @Override
    public void onPermissionGranted() {
    }

    @Override
    public void onPermissionDenied() {
    }
}
