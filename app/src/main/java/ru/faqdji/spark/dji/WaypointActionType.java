package ru.faqdji.spark.dji;

public enum WaypointActionType {
    STAY(0),
    START_TAKE_PHOTO(1),
    START_RECORD(2),
    STOP_RECORD(3),
    ROTATE_AIRCRAFT(4),
    GIMBAL_PITCH(5),
    CAMERA_ZOOM(7),
    CAMERA_FOCUS(8);

    private int value;

    WaypointActionType(int var3) {
        value=var3;
    }

    public int value() {
        return value;
    }
}
