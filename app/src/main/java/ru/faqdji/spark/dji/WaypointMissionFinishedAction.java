package ru.faqdji.spark.dji;

public enum WaypointMissionFinishedAction {
    NO_ACTION(0),
    GO_HOME(1),
    AUTO_LAND(2),
    GO_FIRST_WAYPOINT(3),
    CONTINUE_UNTIL_END(4);

    private int value;

    WaypointMissionFinishedAction(int var3) {
        value=var3;
    }

    public int value() {
        return value;
    }
}
