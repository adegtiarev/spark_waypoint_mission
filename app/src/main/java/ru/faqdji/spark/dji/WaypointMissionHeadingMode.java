package ru.faqdji.spark.dji;

public enum WaypointMissionHeadingMode {
    AUTO(0),
    USING_INITIAL_DIRECTION(1),
    CONTROL_BY_REMOTE_CONTROLLER(2),
    USING_WAYPOINT_HEADING(3),
    TOWARD_POINT_OF_INTEREST(4);

    private int value;

    WaypointMissionHeadingMode(int var3) {
        value=var3;
    }

    public int value() {
        return value;
    }
}
