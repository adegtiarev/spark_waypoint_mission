package ru.faqdji.spark.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.faqdji.spark.db.entity.MissionEntity;

@Dao
public interface MissionEntityDao {

    @Insert
    void insert(MissionEntity missionEntity);

    @Update
    void update(MissionEntity missionEntity);

    @Delete
    void delete(MissionEntity missionEntity);

    @Query("select * from mission order by dateUpdated desc")
    LiveData<List<MissionEntity>> getAll();

    @Query("select * from mission where id=:id")
    MissionEntity findById(String id);
}
