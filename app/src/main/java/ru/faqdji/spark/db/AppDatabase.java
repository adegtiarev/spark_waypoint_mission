package ru.faqdji.spark.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.faqdji.spark.db.dao.MissionEntityDao;
import ru.faqdji.spark.db.dao.MissionPointDao;
import ru.faqdji.spark.db.dao.PointActionDao;
import ru.faqdji.spark.db.entity.MissionEntity;
import ru.faqdji.spark.db.entity.MissionPoint;
import ru.faqdji.spark.db.entity.PointAction;

@Database(
        entities = {
                MissionEntity.class,
                MissionPoint.class,
                PointAction.class,
        },
        version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MissionEntityDao getMissionsDao();

    public abstract MissionPointDao getMissionPointDao();

    public abstract PointActionDao getPointActionDao();
}
