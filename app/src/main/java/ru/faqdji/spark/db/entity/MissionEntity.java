package ru.faqdji.spark.db.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "mission")
public class MissionEntity {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private long dateCreated;
    private long dateUpdated;

    private float speed;
    private int finishedAction;
    private int headingMode;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(long dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getFinishedAction() {
        return finishedAction;
    }

    public void setFinishedAction(int finishedAction) {
        this.finishedAction = finishedAction;
    }

    public int getHeadingMode() {
        return headingMode;
    }

    public void setHeadingMode(int headingMode) {
        this.headingMode = headingMode;
    }
}
