package ru.faqdji.spark.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.faqdji.spark.db.entity.MissionPoint;

@Dao
public interface MissionPointDao {
    @Insert
    void insert(MissionPoint missionPoint);

    @Update
    void update(MissionPoint missionPoint);

    @Delete
    void delete(MissionPoint missionPoint);

    @Delete
    void deletePoints(List<MissionPoint> missionPoints);

    @Query("SELECT * FROM missionPoint where missionId=:missionId order by position")
    List<MissionPoint> getAllByMission(String missionId);
}
