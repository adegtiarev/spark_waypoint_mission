package ru.faqdji.spark.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import ru.faqdji.spark.db.entity.PointAction;

@Dao
public interface PointActionDao {
    @Insert
    void insert(PointAction pointAction);

    @Insert
    void insertAll(List<PointAction> pointActions);

    @Update
    void update(PointAction pointAction);

    @Delete
    void delete(PointAction pointAction);

    @Delete
    void deleteActions(List<PointAction> pointAction);

    @Query("SELECT * FROM pointAction where pointId=:pointId order by position")
    List<PointAction> getAllByPoint(String pointId);

    @Query("SELECT * FROM pointAction")
    LiveData<List<PointAction>> getAll();

    @Query("SELECT * FROM pointAction where pointId in (SELECT id from missionPoint where missionId=:missionId)")
    List<PointAction> getAllByMissionId(String missionId);
}
