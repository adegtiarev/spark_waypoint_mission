package ru.faqdji.spark;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import ru.faqdji.spark.db.AppDatabase;

public class App extends Application {
    private AppDatabase database;
    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "spark-missions-db").build();
        Fabric.with(this, new Crashlytics());
    }

    public static App getInstance() {
        return app;
    }

    public AppDatabase getDatabase() {
        return database;
    }
}
